#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import codecs
import json
from louis import translate, backTranslate
from louis import dotsIO, ucBrl

def dumps(obj): return json.dumps(obj, ensure_ascii=False)

parser = argparse.ArgumentParser(description='LibLouis Commandline Translator.')
parser.add_argument('-t', '--table', required=True, metavar='braille_TABLE_NAME',
        help='Translate using given braille table.')
parser.add_argument('-i', '--infile', required=True, metavar='INPUT_FILE',
        help='Translate each line of this file.')

args = parser.parse_args()

tables = [args.table]
with codecs.open(args.infile, 'r', 'utf-8') as f:
    stop = False
    while not stop:
        line = f.readline().rstrip('\n')
        if line == '':
            stop = True
            continue
        brl = translate(tables, line, None, 0, dotsIO|ucBrl)[0]
        #back = backTranslate(tables, ud(line), None, 0, 0)[0]
        print(brl)
