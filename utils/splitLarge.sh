#!/bin/bash
tmp=$(mktemp)
HEAD=$(mktemp)
TAIL=$(mktemp)

head -n -1 $1 | tail -n +2 >$tmp
split -l 500 -d $tmp
rename "s/x(\d+)/$1.\$1.txt/g" x*
echo "{" >$HEAD
echo "}" >$TAIL
ls -1 $1.*.txt | while read file; do
sed -i '$ s/],$/]/g' $file
cat $HEAD $file $TAIL >$tmp
mv $tmp $file
done
rm $HEAD $TAIL
