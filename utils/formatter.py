#!/usr/bin/env python3
# encoding: utf-8
import argparse
import codecs
import json

def dumps(obj): return json.dumps(obj, ensure_ascii=False)

parser = argparse.ArgumentParser(description='LibLouis Harness Manager, reformatter.')
parser.add_argument('-w', '--wordfile', required=True)
parser.add_argument('-b', '--brlfile', required=True)

args = parser.parse_args()

with codecs.open(args.wordfile, 'r', 'utf-8') as f1, codecs.open(args.brlfile, 'r', 'utf-8') as f2:
    stop = False
    data = []
    while not stop:
        line = f1.readline().rstrip('\n')
        brl = f2.readline().rstrip('\n')
        if line == '':
            stop = True
            continue
        data.append("{w}: {b}".format(w=dumps(line), b=dumps(brl)))
    res = ",\n".join(data)
    if res:
        print("\n".join(['{', res, '}']))
    else:
        print("{\n}")
