#!/usr/bin/env python3
# encoding: utf-8
import unittest
import sqlalchemy as _sa
from sqlalchemy.orm.exc import NoResultFound
from model import *
from app import App, associate


class TestSchema(unittest.TestCase):

    def setUp(self):
        self.app = App('sqlite:///:memory:')
        self.s = self.app.Session()

    def tearDown(self):
        self.s.close()
        Base.metadata.drop_all(self.app.db)

    def test_sym_word_table(self):
        hello = Word.getOrCreate(self.s, 'hello')
        tbl = BrlTable.getOrCreate(self.s, 'en_GB')
        tbl.words.append(hello)
        self.s.add(tbl)
        self.s.commit()
        self.assertEqual(hello.tables[0], tbl)

    def test_sym_braille_word(self):
        w = Word.getOrCreate(self.s, 'hello')
        brl = Braille.getOrCreate(self.s, '⠓⠑⠇⠇⠕')
        w.brailledAs.append(brl)
        self.s.add(w)
        self.s.commit()
        self.assertEqual(brl.words[0], w)

    def test_sym_braille_producer(self):
        brl = Braille.getOrCreate(self.s, '⠓⠑⠇⠇⠕')
        human = Producer.getOrCreate(self.s, 'human')
        brl.producers.append(human)
        self.s.add(brl)
        self.s.commit()
        self.assertEqual(human.productions[0], brl)

    def test_sym_braille_table(self):
        brl = Braille.getOrCreate(self.s, '⠓⠑⠇⠇⠕')
        tbl = BrlTable.getOrCreate(self.s, 'en_GB')
        brl.tables.append(tbl)
        self.s.add(brl)
        self.s.commit()
        self.assertEqual(tbl.translations[0], brl)

    def test_getMismatchingProductions(self):
        # One word, one braille production.
        human = Producer.getOrCreate(self.s, 'human')
        ueb = BrlTable.getOrCreate(self.s, 'ueb')
        hello = Word.getOrCreate(self.s, 'hello')
        brl1 = Braille.getOrCreate(self.s, '⠓⠑⠇⠇⠕')
        associate(ueb, hello, brl1, human)
        self.s.add(hello)
        self.s.commit()
        # Expect an empty list, because we havent added any dups.
        self.assertEqual({}, ueb.getMismatchingProductions())

        # Add a second production for the word which conflicts with the first.
        robot = Producer.getOrCreate(self.s, 'robot')
        brl2 = Braille.getOrCreate(self.s, '⠓⠑⠇⠇')
        associate(ueb, hello, brl2, robot)
        self.s.add(hello)
        self.s.commit()
        result = ueb.getMismatchingProductions()
        expected = {hello: sorted([brl1, brl2])}
        self.assertEqual(expected, result)

        # Add an independant word for the same table.
        # Expect to get the same list of conflicts.
        bye = Word.getOrCreate(self.s, 'bye')
        brl3 = Braille.getOrCreate(self.s, '⠃⠽⠑')
        associate(ueb, bye, brl3, human)
        self.s.add(bye)
        self.s.commit()
        result = ueb.getMismatchingProductions()
        expected = {hello: sorted([brl1, brl2])}
        self.assertEqual(expected, result)

        # Add a second conflict for a different table.
        sv = BrlTable.getOrCreate(self.s, 'sv')
        hej = Word.getOrCreate(self.s, 'hej')
        brl4 = Braille.getOrCreate(self.s, '⠓⠑⠚')
        brl5 = Braille.getOrCreate(self.s, '⡓⠑⠚')
        associate(sv, hej, brl4, human)
        associate(sv, hej, brl5, robot)
        self.s.add(hej)
        self.s.commit()
        # still only expect the first conflict
        # because the query is still against the first table.
        result = ueb.getMismatchingProductions()
        expected = {hello: sorted([brl1, brl2])}
        self.assertEqual(expected, result)
        # Get the second conflict correct table this time.
        result = sv.getMismatchingProductions()
        expected = {hej: sorted([brl4, brl5])}
        self.assertEqual(expected, result)

        # When a production is accepted, it should not show up as a conflict anymore.
        act1 = Accept(ueb, hello, brl1)
        self.s.add(act1)
        self.s.commit()
        self.assertEqual({}, ueb.getMismatchingProductions())

    def test_dumpsMismatchingProductions(self):

        # One word, one braille production.
        human = Producer.getOrCreate(self.s, 'human')
        ueb = BrlTable.getOrCreate(self.s, 'ueb')
        hello = Word.getOrCreate(self.s, 'hello')
        brl1 = Braille.getOrCreate(self.s, '⠓⠑⠇⠇⠕')
        associate(ueb, hello, brl1, human)
        self.s.add(hello)
        self.s.commit()
        result = ueb.dumpsMismatchingProductions()
        # Expect an empty json dict, because we havent added any dups.
        self.assertEqual('{\n}', result)

        # Add a second production for the word which conflicts with the first.
        robot = Producer.getOrCreate(self.s, 'robot')
        brl2 = Braille.getOrCreate(self.s, '⠓⠑⠇⠇')
        associate(ueb, hello, brl2, robot)
        self.s.add(hello)
        self.s.commit()
        result = ueb.dumpsMismatchingProductions()
        expected = '{\n"hello": [0, [ "⠓⠑⠇⠇", "⠓⠑⠇⠇⠕" ], "" ]\n}'
        self.assertEqual(expected, result)

        bye = Word.getOrCreate(self.s, 'bye')
        brl3 = Braille.getOrCreate(self.s, '⠃⠽⠑')
        associate(ueb, bye, brl3, human)
        brl4 = Braille.getOrCreate(self.s, '⠃⠽⠑ :)')
        associate(ueb, bye, brl4, robot)
        self.s.add(bye)
        self.s.commit()
        result = ueb.dumpsMismatchingProductions()
        expected = ('{\n"bye": [0, [ "⠃⠽⠑", "⠃⠽⠑ :)" ], "" ],\n'
                    '"hello": [0, [ "⠓⠑⠇⠇", "⠓⠑⠇⠇⠕" ], "" ]\n}')
        self.assertEqual(expected, result)

    def test_processMismatchingProductions(self):
        ueb = BrlTable.getOrCreate(self.s, 'ueb')
        self.s.add(ueb)
        input = '{\n"hello": [0, ["⠓⠑⠇⠇", "⠓⠑⠇⠇⠕"], ""]\n}'

        # Don't expect anything, since no results were marked, i.e. num==0
        self.assertEqual([], ueb.processMismatchingProductions(input))

        # This time an error is raised, because hello and the braille is not in the db for this table.
        input = '{\n"hello": [2, ["⠓⠑⠇⠇", "⠓⠑⠇⠇⠕"], ""]\n}'
        with self.assertRaises(NoResultFound):
            self.assertEqual([], ueb.processMismatchingProductions(input))

        # This time, prepopulate the database with entries,
        # and the function will still not accept the correct translation for hello.
        # because hello does not have a conflict.
        human = Producer.getOrCreate(self.s, 'human')
        hello = Word.getOrCreate(self.s, 'hello')
        brl1 = Braille.getOrCreate(self.s, '⠓⠑⠇⠇⠕')
        associate(ueb, hello, brl1, human)
        self.s.add(hello)
        self.s.commit()
        with self.assertRaises(KeyError):
            self.assertEqual([], ueb.processMismatchingProductions(input))

        # Add a conflicting production for hello.
        # hello will now show up in conflicts, so will not raise an error.
        brl2 = Braille.getOrCreate(self.s, '⠓⠑⠇⠇')
        associate(ueb, hello, brl2, human)
        self.s.add(hello)
        self.s.commit()
        # Accept the correct production of hello.
        result = ueb.processMismatchingProductions(input)
        self.assertEqual(result[0].word, hello)
        self.assertEqual(result[0].braille, brl1)

    def test_getAccepted(self):
        human = Producer.getOrCreate(self.s, 'human')
        robot = Producer.getOrCreate(self.s, 'robot')
        ueb = BrlTable.getOrCreate(self.s, 'ueb')
        hello = Word.getOrCreate(self.s, 'hello')
        brl1 = Braille.getOrCreate(self.s, '⠓⠑⠇⠇⠕')
        brl2 = Braille.getOrCreate(self.s, '⠓⠑⠇⠇')
        associate(ueb, hello, brl1, human)
        associate(ueb, hello, brl2, robot)

        bye = Word.getOrCreate(self.s, 'bye')
        brl3 = Braille.getOrCreate(self.s, '⠃⠽⠑')
        associate(ueb, bye, brl3, human)

        act1 = Accept(ueb, hello, brl1)
        act2 = Accept(ueb, bye, brl3)
        self.s.add_all([hello, bye, act1, act2])
        self.s.commit()
        # sorted in alpha order
        expected = sorted([act1, act2])
        self.assertEqual(expected, ueb.getAccepted())

        # Add another accepted word, but for a different table.
        sv = BrlTable.getOrCreate(self.s, 'sv')
        hej = Word.getOrCreate(self.s, 'hej')
        brl4 = Braille.getOrCreate(self.s, '⠓⠑⠚')
        associate(sv, hej, brl4, human)
        act3 = Accept(sv, hej, brl4)
        self.s.add_all([hej, act3])
        self.s.commit()

        expected = [act3]
        self.assertEqual(expected, sv.getAccepted())

    def test_dumpsAccepted(self):
        ueb = BrlTable.getOrCreate(self.s, 'ueb')
        self.s.add(ueb)
        self.assertEqual('[\n]', ueb.dumpsAccepted())

        # One word, one braille production.
        human = Producer.getOrCreate(self.s, 'human')
        hello = Word.getOrCreate(self.s, 'hello')
        brl1 = Braille.getOrCreate(self.s, '⠓⠑⠇⠇⠕')
        associate(ueb, hello, brl1, human)
        self.s.add_all([hello, human, brl1, ueb, Accept(ueb, hello, brl1)])
        self.s.commit()

        expected = '[\n        { "input": "hello", "output": "⠓⠑⠇⠇⠕" }\n]'
        self.assertEqual(expected, ueb.dumpsAccepted())

    def test_loadWords(self):
        en = BrlTable.getOrCreate(self.s, 'en')
        self.s.add(en)
        en.loadWords('hi\nhello\nbye')
        self.s.commit()
        hi = Word.getOrCreate(self.s, 'hi')
        hello = Word.getOrCreate(self.s, 'hello')
        bye = Word.getOrCreate(self.s, 'bye')
        expected = sorted([hi, hello, bye])
        result = sorted(en.words)
        self.assertEqual(expected, result)

    def test_loadWordBrailledAs(self):
        liblouis = Producer.getOrCreate(self.s, 'liblouis')
        en = BrlTable.getOrCreate(self.s, 'en')
        self.s.add_all([en, liblouis])
        input = '{ "hello": "⠓⠑⠇⠇⠕", "bye": "⠃⠽⠑"}'
        en.loadWordBrailledAs(liblouis, input)
        hello = Word.getOrCreate(self.s, 'hello')
        brl1 = Braille.getOrCreate(self.s, '⠓⠑⠇⠇⠕')
        self.assertEqual(hello.brailledAs, [brl1])
        self.assertEqual(brl1.producers, [liblouis])
        self.assertEqual(hello.tables, [en])

if __name__ == '__main__':
    unittest.main()
