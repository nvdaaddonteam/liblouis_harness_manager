#!/bin/bash
set -e

tmp=/tmp/tmp.txt

function process () {
tableName=$1
producer=$2
wordlist=$3
production=$4

echo "processing $tableName, $producer $wordlist $production"
echo "creating table"
./manager  --add_table $tableName
echo "formatting wordlist and production"
./utils/formatter.py -w $wordlist -b $production >$tmp
echo "adding producer"
./manager  --add_producer $producer
echo "loading production data"
./manager --load_wordBrailledAs $tableName $producer $tmp
echo "all done"
rm $tmp
}

function retranslate() {
tableName=$1
wordlist=$2
output=$3
./utils/lou_translate.py -i $wordlist -t $tableName >$output
}

function process_k90 () {
tablename=$1
extension=$2
producer=$3

process $tablename $producer raw/01-k90.txt raw/01-k90_${producer}_${tablename}.txt
latest=/tmp/01-k90_liblouis_${tablename}.txt
retranslate ${tablename}.${extension} raw/01-k90.txt $latest
process $tablename ll-latest raw/01-k90.txt $latest
}

process_k90 da-dk-g18 utb inhouse
process_k90 da-dk-g28 ctb inhouse
process_k90 da-dk-g16 utb inhouse
process_k90 da-dk-g26 ctb inhouse
