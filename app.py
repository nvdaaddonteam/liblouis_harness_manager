#!/usr/bin/env python3
# encoding: utf-8
import json
import sqlalchemy as _sa
import sqlalchemy.orm as _orm

from sqlalchemy.orm.exc import NoResultFound

from model import Accept, Braille, Producer, BrlTable, Word
from model import Base


def associate(tbl, word, brl, producer):
    tbl.words.append(word)
    word.brailledAs.append(brl)
    brl.tables.append(tbl)
    brl.producers.append(producer)

class App(object):

    def __init__(self, uri='sqlite:///database.db'):
        self.uri = uri
        self.db = _sa.create_engine(self.uri, echo=False, encoding='UTF-8')
        self.Session = _orm.sessionmaker(bind=self.db)
        Base.metadata.create_all(self.db)
