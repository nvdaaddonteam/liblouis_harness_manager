from collections import defaultdict
import json
import sqlalchemy as _sa
import sqlalchemy.orm as _orm
import sqlalchemy.orm.exc as _ormexc
import sqlalchemy.exc as _exc
import sqlalchemy.ext.declarative as _declarative


Base = _declarative.declarative_base()

def dumps(obj):
    return json.dumps(obj, ensure_ascii=False)

def createAssociationTable(name1, name2):
    name1ref = "%s.string" % name1
    name2ref = "%s.string" % name2
    tblname = "%s_%s_assoc" % (name1, name2)
    uniqname = "%s_%s_uic" % (name1, name2)
    return _sa.Table(tblname, Base.metadata,
        _sa.Column(name1, _sa.String, _sa.ForeignKey(name1ref)),
        _sa.Column(name2, _sa.String, _sa.ForeignKey(name2ref)),
        _sa.UniqueConstraint(name1, name2, name=uniqname))


class _Stringer(_declarative.AbstractConcreteBase, Base):
    string = _sa.Column(_sa.String, primary_key=True)

    @_declarative.declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    def __init__(self, string, *args, **kwargs):
        super(_Stringer, self).__init__(*args, **kwargs)
        self.string = string

    def __lt__(self, other):
        if isinstance(other, _Stringer):
            return self.string < other.string
        return NotImplemented

    def __gt__(self, other):
        if isinstance(other, _Stringer):
            return self.string > other.string
        return NotImplemented

    def __repr__(self):
        return "<{name}('{value}')>".format(
            name=self.__class__.__name__,
            value=self.string
        )

    def __str__(self):
        return self.string


    @classmethod
    def getIfExists(cls, session, string):
        try:
            return session.query(cls).filter(cls.string == string).one()
        except Exception as e:
            if not e.args: e.args=('',)
            msg = "{s} in {t}, {o}".format(o=e.args[0], s=string, t=cls.__name__)
            e.args = (msg,) + e.args[1:]
            raise

    @classmethod
    def getOrCreate(cls, session, string):
        try:
            return cls.getIfExists(session, string)
        except _ormexc.NoResultFound:
            return cls(string)


_word_table_assoc = createAssociationTable('word', 'brltable')
_word_braille_assoc = createAssociationTable('word', 'braille')
_word_hyphenation_assoc = createAssociationTable('word', 'hyphenation')
_producer_braille_assoc = createAssociationTable('producer', 'braille')
_producer_hyphenation_assoc = createAssociationTable('producer', 'hyphenation')
_table_braille_assoc = createAssociationTable('brltable', 'braille')


class Word(_Stringer):
    brailledAs = _orm.relationship(
        "Braille", secondary=_word_braille_assoc, backref="words")
    tables = _orm.relationship(
        "BrlTable", secondary=_word_table_assoc, backref="words")
    hyphenatedAs = _orm.relationship(
        "Hyphenation", secondary=_word_hyphenation_assoc, backref="words")


class BrlTable(_Stringer):
    translations = _orm.relationship(
        "Braille", secondary=_table_braille_assoc, backref="tables")

    def getAccepted(self, specificWord=None):
        session = _sa.inspect(self).session
        if not session:
            raise _exc.UnboundExecutionError('Need to be bound to a session')
        records = []
        lst = [specificWord] if specificWord else sorted(self.words)
        for word in lst:
            rows = session.query(Accept).filter(
                    _sa.and_(Accept.brltable == self, Accept.word == word)).all()
            records.extend(rows)
        return records

    def dumpsAccepted(self):
        accepted = self.getAccepted()
        strings = []
        for act in accepted:
            strings.append('        { "input": %s, "output": %s }' % (dumps(act.word.string), dumps(act.braille.string)))
        resString = ",\n".join(strings)
        if resString:
            return "\n".join(['[', resString, ']'])
        return "[\n]"

    def getMismatchingProductions(self):
        session = _sa.inspect(self).session
        if not session:
            raise _exc.UnboundExecutionError('Need to be bound to a session')
        myTableName=self.string
        mwt = _word_table_assoc
        mwb = _word_braille_assoc
        mtb = _table_braille_assoc
        qTblBrl = _sa.select([
            mtb.c.brltable.label('brltable'),
            mtb.c.braille.label('braille')]
        ).where(mtb.c.brltable==myTableName).distinct(mtb.c.braille)
        # if we don't alias the query, we get an error, column 'table' ambiguous.
        qTblBrla = qTblBrl.alias()
        sel1 = _sa.select([qTblBrla.c.brltable, mwb], qTblBrla.c.braille ==mwb.c.braille).distinct()
        res = session.execute(sel1).fetchall()
        mydict = defaultdict(list)
        for r in res:
            mydict[r.word].append(r.braille)
        mismatches = {}
        allActs = session.query(Accept).filter(Accept.brltable == self).all()
        for wordStr, transcriptionsStr in mydict.items():
            if len(transcriptionsStr) == 1: continue
            word = Word.getIfExists(session, wordStr)
            acts = [act_ for act_ in allActs if act_.word == word]
            if not len(acts):
                mismatches[word] = [Braille.getIfExists(session, brlStr) for brlStr in sorted(transcriptionsStr)]
        return mismatches

    def dumpsMismatchingProductions(self):
        conflicts = self.getMismatchingProductions()
        strings = []
        for word, transcriptions in sorted(conflicts.items()):
            s = ", ".join([dumps(brl.string) for brl in transcriptions])
            strings.append('{w}: [0, [ {l} ], "" ]'.format(w=dumps(word.string), l=s))
        resString = ",\n".join(strings)
        if resString:
            return "\n".join(['{', resString, '}'])
        return "{\n}"

    def dumpsMatchingProductions(self):
        session = _sa.inspect(self).session
        if not session:
            raise _exc.UnboundExecutionError('Need to be bound to a session')
        myTableName=self.string
        mwt = _word_table_assoc
        mwb = _word_braille_assoc
        mtb = _table_braille_assoc
        qTblBrl = _sa.select([
            mtb.c.brltable.label('brltable'),
            mtb.c.braille.label('braille')]
        ).where(mtb.c.brltable==myTableName).distinct(mtb.c.braille)
        # if we don't alias the query, we get an error, column 'table' ambiguous.
        qTblBrla = qTblBrl.alias()
        sel1 = _sa.select([qTblBrla.c.brltable, mwb], qTblBrla.c.braille ==mwb.c.braille).distinct()
        res = session.execute(sel1).fetchall()
        mydict = defaultdict(list)
        for r in res:
            mydict[r.word].append(r.braille)
        strings = []
        append = strings.append
        for wordStr, transcriptionsStr in sorted(mydict.items()):
            if len(transcriptionsStr) != 1: continue
            append('{ "input": %s, "output": %s }' %(dumps(wordStr), dumps(transcriptionsStr[0])))
        return ",\n".join(strings)

    def loadWords(self, oneWordPerLine):
        session = _sa.inspect(self).session
        if not session:
            raise _exc.UnboundExecutionError('Need to be bound to a session')
        strings = oneWordPerLine.split('\n')
        try:
            strings.remove('')
        except ValueError:
            pass
        myTableName = self.string
        wordIns = Word.__table__.insert().prefix_with("OR REPLACE")
        wordTblIns = _word_table_assoc.insert().prefix_with("OR REPLACE")
        session.execute(wordIns, [{'string': _} for _ in strings])
        session.execute(wordTblIns, [{'word':string, 'brltable':myTableName} for string in strings])

    def loadWordBrailledAs(self, producer, jsonData):
        session = _sa.inspect(self).session
        if not session:
            raise _exc.UnboundExecutionError('Need to be bound to a session')
        data = json.loads(jsonData)

        wordIns = Word.__table__.insert().prefix_with("OR REPLACE")
        brlIns = Braille.__table__.insert().prefix_with("OR REPLACE")
        wordBrlIns = _word_braille_assoc.insert().prefix_with("OR REPLACE")
        wordTblIns = _word_table_assoc.insert().prefix_with("OR REPLACE")
        brlTblIns = _table_braille_assoc.insert().prefix_with("OR REPLACE")
        brlProdIns = _producer_braille_assoc.insert().prefix_with("OR REPLACE")

        session.execute(wordIns, [{'string': word} for word in data])
        session.execute(brlIns, [{'string': brl} for word,brl in data.items()])
        session.execute(wordBrlIns, [{'word':word, 'braille':brl} for word, brl in data.items()])
        myTableName = self.string
        session.execute(wordTblIns, [{'word':word, 'brltable':myTableName} for word in data])
        session.execute(brlTblIns, [{'brltable':myTableName, 'braille':brl} for word, brl in data.items()])
        myProducerName = producer.string
        session.execute(brlProdIns, [{'producer':myProducerName, 'braille':brl} for word, brl in data.items()])


    def processMismatchingProductions(self, jsonData):
        resolved = []
        session = _sa.inspect(self).session
        if not session:
            raise _exc.UnboundExecutionError('Need to be bound to a session')
        conflicts = self.getMismatchingProductions()
        data = json.loads(jsonData)
        for (wordStr, [num, opts, comment]) in data.items():
            if not num: continue
            num -= 1
            if not (0 <= num and num < len(opts)):
               raise IndexError('List index out of range 0<=x<%d, got %d.' % (len(opts), num))
            word = Word.getIfExists(session, wordStr)
            brl = Braille.getIfExists(session, opts[num])
            if word not in conflicts or brl not in conflicts[word]:
                raise KeyError('no such word/braille combo (%s, %s), for this table.' % (word.string, brl.string))
            resolved.append(Accept(self, word, brl, comment=comment))
        return resolved


class Producer(_Stringer):
    productions = _orm.relationship(
        "Braille", secondary=_producer_braille_assoc, backref="producers")
    hyphenations = _orm.relationship(
        "Hyphenation", secondary=_producer_hyphenation_assoc, backref="producers")

class Braille(_Stringer):
    pass

class Hyphenation(_Stringer):
    pass

class Accept(Base):
    __tablename__ = 'accepted'
    brltable_id = _sa.Column(_sa.String, _sa.ForeignKey('brltable.string'), primary_key=True)
    brltable = _orm.relationship("BrlTable")
    word_id = _sa.Column(_sa.String, _sa.ForeignKey('word.string'), primary_key=True)
    word = _orm.relationship("Word")
    braille_id = _sa.Column(
        _sa.String, _sa.ForeignKey('braille.string'))
    braille = _orm.relationship("Braille")
    direction = _sa.Column(_sa.Enum('both', 'fwrd', 'back'), primary_key=True)
    comment = _sa.Column(_sa.String)

    def __init__(self, tbl, word, brl, direction='both', comment=''):
        self.brltable = tbl
        self.word = word
        self.braille = brl
        self.direction = direction
        self.comment = comment

    def __lt__(self, other):
        if isinstance(other, Accept):
            return self.word < other.word
        return NotImplemented

    def __gt__(self, other):
        if isinstance(other, Accept):
            return self.word > other.word
        return NotImplemented

    def __repr__(self):
        return "<{name}('{tbl}':'{word}':'{brl}')>".format(
            name=self.__class__.__name__,
            tbl=self.brltable.string,
            word=self.word.string,
            brl=self.braille.string
        )

# vim: foldlevel=0
